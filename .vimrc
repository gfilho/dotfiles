set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'
Plugin 'Valloric/YouCompleteMe'
Plugin 'vim-syntastic/syntastic'
Plugin 'scrooloose/nerdtree'
Plugin 'octol/vim-cpp-enhanced-highlight'
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'tpope/vim-fugitive'
Plugin 'vim-scripts/Conque-GDB'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required

" Put your non-Plugin stuff after this line

" General
set enc=utf-8
set fenc=utf-8
set termencoding=utf-8
set nocompatible
" use indentation of previous line
" set autoindent
" use intelligent indentation for C
" set smartindent
" configure tabwidth and insert spaces instead of tabs
set tabstop=2        " tab width is 4 spaces
set shiftwidth=2     " indent also with 4 spaces
set expandtab        " expand tabs to spaces
set textwidth=0
set wrapmargin=0
set wrap
set linebreak
set nolist  " list disables linebreak
set t_Co=256
set number

highlight ExtraWhitespace ctermbg=red guibg=required
match ExtraWhitespace /\s\+$/

syntax on

" YouCompleteMe
let g:ycm_global_ycm_extra_conf = '~/.vim/.ycm_extra_conf.py'
" let g:ycm_collect_identifiers_from_tags_files = 1

" Airline
let g:airline_powerline_fonts=1
let g:airline_theme='badwolf'

" NERDTRee
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | vertical resize +20 | endif

" Enhanced Higlights
let g:cpp_concepts_highlight = 1

if has("cscope")

 nmap <C-\>s :cs find s <C-R>=expand("<cword>")<CR><CR> 
              set csprg=/usr/bin/cscope
  set csprg=/usr/bin/cscope
  set csto=0
  set cst
  set csverb
  " C symbol
  nmap <C-\>s :cs find s <C-R>=expand("<cword>")<CR><CR>
  " definition
  nmap <C-\>g :cs find g <C-R>=expand("<cword>")<CR><CR>
  " functions that called by this function
  nmap <C-\>d :cs find d <C-R>=expand("<cword>")<CR><CR>
  " funtions that calling this function
  nmap <C-\>c :cs find c <C-R>=expand("<cword>")<CR><CR>
  " test string
  nmap <C-\>t :cs find t <C-R>=expand("<cword>")<CR><CR>
  " egrep pattern
  nmap <C-\>e :cs find e <C-R>=expand("<cword>")<CR><CR>
  " file
  nmap <C-\>f :cs find f <C-R>=expand("<cfile>")<CR><CR>
  " files #including this file
  nmap <C-\>i :cs find i ^<C-R>=expand("<cfile>")<CR>$<CR>

  " Automatically make cscope connections
  function! LoadCscope()
      let db = findfile("cscope.out", ".;")
      if (!empty(db))
          let path = strpart(db, 0, match(db, "/cscope.out$"))
          set nocscopeverbose " suppress 'duplicate connection' error
          exe "cs add " . db . " " . path
          set cscopeverbose
      endif
  endfunction
  au BufEnter /* call LoadCscope()

endif


let g:ConqueTerm_Color = 2         " 1: strip color after 200 lines, 2: always with color
let g:ConqueTerm_CloseOnEnd = 1    " close conque when program ends running
let g:ConqueTerm_StartMessages = 0 " display warning messages if conqueTerm is configured incorrectly

